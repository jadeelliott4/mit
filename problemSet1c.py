#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  1 14:29:37 2018

@author: elliotj
"""

portion_down_payment = 0.25
current_savings = 0
r = 0.04
counter = 0
semi_anual_raise = 0.07
portion_needed = 250000
low = 0
high = 10000
annual_salary = float(input("Enter your anual salary: "))
portion_saved = (low + high)/2

while abs(current_savings - portion_needed) > 100 and counter <=36:
    portion_saved = portion_saved/10000
    monthly = current_savings * r/12
    monthly_saved = annual_salary * portion_saved/12
    current_savings += monthly + monthly_saved
    if counter%6 == 0:
        annual_salary += annual_salary * semi_anual_raise
        
    if current_savings < portion_needed:
        low = portion_saved
        
    else:
        high = portion_saved
    portion_saved = (high + low)/20000
    counter += 1
        
print(f"Best savings rate: {portion_saved}")    
print (f"Number of months: {counter}")