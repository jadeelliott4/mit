import math
import random
import string
import collections

VOWELS = 'aeiou'
CONSONANTS = 'bcdfghjklmnpqrstvwxyz'
HAND_SIZE = 7

SCRABBLE_LETTER_VALUES = {
    'a': 1, 'b': 3, 'c': 3, 'd': 2, 'e': 1, 'f': 4, 'g': 2, 'h': 4, 'i': 1, 'j': 8, 'k': 5, 'l': 1, 'm': 3, 'n': 1, 'o': 1, 'p': 3, 'q': 10, 'r': 1, 's': 1, 't': 1, 'u': 1, 'v': 4, 'w': 4, 'x': 8, 'y': 4, 'z': 10, '*':0}

WORDLIST_FILENAME = "words.txt"

def load_words():
    
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # wordlist: list of strings
    wordlist = []
    for line in inFile:
        wordlist.append(line.strip().lower())
    print("  ", len(wordlist), "words loaded.")
    return wordlist



def get_frequency_dict(sequence):

    freq = {}
    for x in sequence:
        freq[x] = freq.get(x,0) + 1
    return freq
	


def get_word_score(word, n):

    sum1 = 0;
    lowerWord = word.lower()
    wordValue= 0
    word_length = len(word)
    if word == "!!":
        score = 0
    else:
        for letter in lowerWord:
            wordValue += SCRABBLE_LETTER_VALUES.get(letter)
        component = 7 * word_length - 3 * (n - word_length)
        if component > 1:
            sum1 = component
        else:
            sum1 = 1
        score = wordValue * sum1
    return score



def display_hand(hand):
    print("Available hand: ")
    for letter in hand.keys():
        for j in range(hand[letter]):
             print(letter, end=' ')      # print all on the same line
    print()                              # print an empty line



def deal_hand(n):

    hand={}
    num_vowels = int(math.ceil(n / 3))

    for i in range(num_vowels - 1):
        x = random.choice(VOWELS)
        hand[x] = hand.get(x, 0) + 1

    
    for i in range(num_vowels, n):    
        x = random.choice(CONSONANTS)
        hand[x] = hand.get(x, 0) + 1
        
    hand["*"] = hand.get("*", 0) + 1
    
    return hand



def update_hand(hand, word):

    word_count = collections.Counter(word)
    for char in word:
        for letter, number in hand.items():
            if letter == char:
                hand[letter] = hand.get(char, 0) - word_count.get(char, 0)
                if hand[letter] < 0:
                    hand[letter] = 0
    return(hand)            


def is_valid_word(word, hand, word_list):
        
    new_hand = hand.copy()
    word = word.lower()
    if '*' in word:
        for i in VOWELS:
            newWord = word.replace('*', i)
            if newWord in word_list:
                return True
                break
    elif word in word_list:
        for letter in word:
            if (letter not in new_hand) or (new_hand.get(letter) == 0):    
                return False
            else:
                new_hand[letter] = new_hand.get(letter) - 1
        return True
    else:
        return False 


def calculate_handlen(hand):
    
    total=0
    for i in hand:
        total += hand[i]    
    return total

def play_hand(hand, word_list):

    word = ""
    numpoints = 0
    totalpoints = 0
    display_hand(hand)
    substitute = input("Would you like to substitute a letter? ")
    if substitute == 'no':
        display_hand(hand)
    elif substitute == 'yes':
        letter = input("Which letter would you like to change? ")
        substitute_hand(hand, letter)
        display_hand(hand)
    while not all(x==0 for x in hand.values()):
        n = len(hand)
        n = sum(hand.values())
        word = input("Enter word, or \"!!\" to indicate that you are finished:")
        if is_valid_word(word, hand, word_list) == True:
            numpoints = get_word_score(word, n)
            totalpoints += numpoints
            print(f"{word} earned {numpoints} points. Total score: {totalpoints}")
            update_hand(hand, word)
            display_hand(hand)
        elif word == "!!":
            print(f"Total score: {totalpoints} points")
            break
        elif is_valid_word(word, hand, word_list) == False:
            print("That is not a valid word")
    else:
        print(f"Ran out of letters. Total score: {totalpoints} points")
        

def substitute_hand(hand, letter):

    if letter in CONSONANTS:    
        hand[random.choice(CONSONANTS)] = hand.pop(letter)
    else:
        hand[random.choice(VOWELS)] = hand.pop(letter)
    return(hand)
       
    
def play_game(word_list):
    
    number_hands = int(input("How many hands do you want to play? "))
    while number_hands > 0:
        hand = deal_hand(HAND_SIZE)
        play_hand(hand, word_list)
        number_hands -= 1 
        

if __name__ == '__main__':
    word_list = load_words()
    play_game(word_list)