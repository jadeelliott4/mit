#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 30 12:23:33 2018

@author: elliotj
"""

portion_down_payment = 0.25
current_savings = 0
r = 0.04
counter = 0
annual_salary = float(input("Enter your anual salary: "))
portion_saved = float(input("Enter the percent of your salary to save, as a decimal: "))
total_cost = float(input("Enter the cost of your dream home: "))
semi_anual_raise = float(input("Enter the semi­annual raise, as a decimal: "))
portion_needed = total_cost * portion_down_payment

while current_savings <= portion_needed:
    monthly = current_savings * r/12
    monthly_saved = annual_salary * portion_saved/12
    current_savings += monthly + monthly_saved
    counter += 1
    if counter%6 == 0:
        annual_salary += annual_salary * semi_anual_raise
print (f"Number of months: {counter}")