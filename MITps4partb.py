import string

### HELPER CODE ###
def load_words(file_name):
    '''
    file_name (string): the name of the file containing 
    the list of words to load    
    
    Returns: a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    '''
    print("Loading word list from file...")
    # inFile: file
    inFile = open(file_name, 'r')
    # wordlist: list of strings
    wordlist = []
    for line in inFile:
        wordlist.extend([word.lower() for word in line.split(' ')])
    print("  ", len(wordlist), "words loaded.")
    return wordlist

def is_word(word_list, word):
    '''
    Determines if word is a valid word, ignoring
    capitalization and punctuation

    word_list (list): list of words in the dictionary.
    word (string): a possible word.
    
    Returns: True if word is in word_list, False otherwise

    Example:
    >>> is_word(word_list, 'bat') returns
    True
    >>> is_word(word_list, 'asdf') returns
    False
    '''
    word = word.lower()
    word = word.strip(" !@#$%^&*()-_+={}[]|\:;'<>?,./\"")
    return word in word_list

def get_story_string():
    """
    Returns: a story in encrypted text.
    """
    f = open("story.txt", "r")
    story = str(f.read())
    f.close()
    return story

### END HELPER CODE ###

WORDLIST_FILENAME = 'words.txt'

class Message(object):
    def __init__(self, text):

        self.message_text = text
        self.valid_words = load_words(WORDLIST_FILENAME)


    def get_message_text(self):

        return self.message_text


    def get_valid_words(self):

        return self.valid_words[:]


    def build_shift_dict(self, shift):

        alphabet = list(string.ascii_lowercase)
        alphabet_val = list(string.ascii_lowercase)
        alphabet_shift = alphabet_val[shift:] + alphabet_val[:shift]
        
        shift_dict = dict(zip(alphabet, alphabet_shift))
        
        upper_alphabet = list(string.ascii_uppercase)
        upper_alphabet_val = list(string.ascii_uppercase)
        upper_alphabet_shift = upper_alphabet_val[shift:] + upper_alphabet_val[:shift]
        
        upper_shift_dict = dict(zip(upper_alphabet, upper_alphabet_shift))
        
        whole_shift_dict = dict(upper_shift_dict, **shift_dict)
        whole_shift_dict[' '] = ' ' 
        
        return(whole_shift_dict)
        
        
    def apply_shift(self, shift):

        shift_string = ''
        dictionary = self.build_shift_dict(shift)
        for char in self.message_text:
            value = dictionary.get(char)
            shift_string += value
        
        return(shift_string)
        
        

class PlaintextMessage(Message):
    def __init__(self, text, shift):

        self.shift = shift
        self.message_text = text
        self.valid_words = load_words(WORDLIST_FILENAME)
        self.encrypting_dict = super(PlaintextMessage, self).build_shift_dict(shift)
        self.message_text_encrypted = super(PlaintextMessage, self).apply_shift(shift)


    def get_shift(self):

        return(self.shift)
                

    def get_encryption_dict(self):

        return(self.encrypting_dict)
                

    def get_message_text_encrypted(self):

        return(self.message_text_encrypted)        
        

    def change_shift(self, shift):

        self.shift = shift
        self.encrypting_dict = super(PlaintextMessage, self).build_shift_dict(shift)
        self.message_text_encrypted = super(PlaintextMessage, self).apply_shift(shift)
        


class CiphertextMessage(Message):
    def __init__(self, text):

        self.message_text = text


    def decrypt_message(self):
        '''
        Decrypt self.message_text by trying every possible shift value
        and find the "best" one. We will define "best" as the shift that
        creates the maximum number of real words when we use apply_shift(shift)
        on the message text. If s is the original shift value used to encrypt
        the message, then we would expect 26 - s to be the best shift value 
        for decrypting it.

        Note: if multiple shifts are equally good such that they all create 
        the maximum number of valid words, you may choose any of those shifts 
        (and their corresponding decrypted messages) to return

        Returns: a tuple of the best shift value used to decrypt the message
        and the decrypted message text using that shift value
        '''
        shift = 1
        encrypted = ""
        while shift < 27:
            for key, value in build_shift_dict(shift):
                for char in self.message_text:
                    if value == char:
                        encrypted += key
                
        
        
