def get_permutations(sequence):

    sequence1 = list(sequence)
    perms = []

    if len(sequence1) == 1:
        return(sequence1)

    for i in range(len(sequence1)):
        
        value = sequence1[i]
        
        remaining_list = sequence1[: i] + sequence1[i+1 :]
        
        for p in get_permutations(remaining_list):
            perms.append([value] + [p])
            
    return(perms)    
    print(perms)
    

