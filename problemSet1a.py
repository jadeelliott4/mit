#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 29 09:45:50 2018

@author: elliotj
"""

portion_down_payment = 0.25
current_savings = 0
r = 0.04
counter = 0
annual_salary = float(input("Enter your anual salary: "))
portion_saved = float(input("Enter the percent of your salary to save, as a decimal: "))
total_cost = float(input("Enter the cost of your dream home: "))
portion_needed = total_cost * portion_down_payment
monthly_saved = annual_salary * portion_saved/12

while current_savings <= portion_needed:
    monthly = current_savings * r/12
    current_savings += monthly + monthly_saved
    counter += 1
    print(annual_salary)
print (f"Number of months: {counter}")