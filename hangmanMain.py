#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  1 15:25:23 2018

@author: elliotj
"""

import random
import string

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist



def choose_word(wordlist):
    """
    wordlist (list): list of words (strings)
    
    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

wordlist = load_words()


def is_word_guessed(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing; assumes all letters are
      lowercase
    letters_guessed: list (of letters), which letters have been guessed so far;
      assumes that all letters are lowercase
    returns: boolean, True if all the letters of secret_word are in letters_guessed;
      False otherwise
    '''
    
    for char in letters_guessed:
        if char in secret_word:
            return True
        else:
            return False
            

def findOccurrences(s, ch):
    return [i for i, letter in enumerate(s) if letter == ch]


def get_guessed_word(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string, comprised of letters, underscores (_), and spaces that represents
      which letters in secret_word have been guessed so far.
    '''
    new_word = secret_word
    list1 = list(new_word)
    for i in range(len(new_word)):
        list1[i] = "_ "
        
    for char in letters_guessed:
        if char in secret_word:
            word_index = findOccurrences(secret_word, char)
            for x in word_index:
                list1[x] = char
    return list1
    print(list1)
            
    

def get_available_letters(letters_guessed):
    '''
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string (of letters), comprised of letters that represents which letters have not
      yet been guessed.
    '''
    letters_guessed = letters_guessed.lower()
    letters_guessed = ''.join(sorted(letters_guessed)) 
    new_list = "abcdefghijklmnopqrstuvwxyz"
    for char in letters_guessed:
        new_list = new_list.replace(char, "")
    return new_list
        

def hangman(secret_word):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many 
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses

    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Remember to make
      sure that the user puts in a letter!
    
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
    
    Follows the other limitations detailed in the problem write-up.
    '''
    word_count = len(secret_word)
    print(f"The word has {word_count} letters")
    total_guessed_letters = ""
    counter = 0
    
    for i in range(0,6):
        num_guesses = 6 - i
        print(f"You have {num_guesses} guesses left")
        letters_guessed = input("> ")  
        if (not letters_guessed.isalpha()) or len(letters_guessed) > 1:
            counter += 1
            warnings = 3 - counter
            print(f"You have not guessed a letter, you have {warnings} warnings")
            if warnings == 0:
                break
        else:    
            for char in letters_guessed:
                if char in total_guessed_letters:
                    print("Oops! You've already guessed that letter ")  
                else:            
                    total_guessed_letters = total_guessed_letters + letters_guessed
                    word = get_guessed_word(secret_word, total_guessed_letters)
                    for char in letters_guessed:
                        if char in secret_word:
                            print("Good guess: ")
                        else:
                            print("Oops! that letter is not in the word: ")
                    word1 = ''.join(word)
                    print(word1)
                    print("----------------------------")
                    available_letters = get_available_letters(total_guessed_letters)
                    print(f"Available letters: {available_letters}" )            
            if word1 == secret_word:
                print("Congratulations you won!")   
                break    
    print(f"The word was {secret_word}")
        
    
if __name__ == "__main__":
    # pass

    # To test part 2, comment out the pass line above and
    # uncomment the following two lines.
    
    secret_word = choose_word(wordlist)
    hangman(secret_word)    